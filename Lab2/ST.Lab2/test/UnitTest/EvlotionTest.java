package UnitTest;

import Functions.Base.Cotangent;
import Functions.Base.NaturalLogarithm;
import Functions.ICalculatable;
import Functions.LogarithmFunc;
import Functions.Logarithmic.Logarithm10Base;
import Functions.Logarithmic.Logarithm3Base;
import Functions.Logarithmic.Logarithm5Base;
import Functions.MainFunc;
import Functions.Trigonometric.Cosecant;
import Functions.Trigonometric.Secant;
import Functions.Trigonometric.Sinus;
import Functions.Trigonometric.Tangent;
import Functions.TrigonometricFunc;
import Infrastructure.Settings;
import Stubs.Logarithm.Logarithm10BaseStub;
import Stubs.Logarithm.Logarithm2BaseStub;
import Stubs.Logarithm.Logarithm3BaseStub;
import Stubs.Logarithm.Logarithm5BaseStub;
import Stubs.Logarithm.LogarithmFuncStub;
import Stubs.Logarithm.NaturalLogarithmStub;
import Stubs.Trigonometric.CosecantStub;
import Stubs.Trigonometric.CotangentStub;
import Stubs.Trigonometric.SecantStub;
import Stubs.Trigonometric.SinusStub;
import Stubs.Trigonometric.TangentStub;
import Stubs.Trigonometric.TrigonometricFuncStub;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class EvlotionTest {
    
    private final double _tolerance = 0.0001;
    private final double startX = 0;
    private final double endX = 10;
    private final double step = 0.5;
    public EvlotionTest() {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
        Settings.getInstance().setNeedSaveAll(true);
    }
    
    @Test
    public void TestAllLevelsStubs() 
    {
        
        ICalculatable function =  new MainFunc(new LogarithmFuncStub(), new TrigonometricFuncStub() );
        Settings.getInstance()._mainFuncCvsName = "alllvl.cvs";
            for (int i=0; i < 10; i+=1)
            {
                System.out.printf("f(%d) = %f\n", i, function.Calculate(GetX(i)));
            }
    }
    
    private double GetX(int i)
    {
        switch (i)
        {
            case 0:
                i++;
                return 9.00001;
            case 1:
                i++;
                return 20;
            case 2:
                i++;
                return 13000;
            case 3:
                i++;
                return 4.71;
            case 4:
                i++;
                return 5.39;
            case 5:
                i++;
                return 6.28;
            case 6:
                i++;
                return 7.18;
            case 7:
                i++;
                return 7.85;
            case 8:
                i++;
                return 7.9;
            case 9:
                i++;
                return 9;
            default:
                return 9;
        }
    }
    
    @Test
    public void TestFirstLevelsStubs() 
    {
        ICalculatable logarithmFunc = new LogarithmFunc(new Logarithm10BaseStub(),
                                                        new Logarithm5BaseStub(),
                                                        new Logarithm3BaseStub(),
                                                        new Logarithm2BaseStub(),
                                                        new NaturalLogarithmStub());
        ICalculatable trigonometricFunc = new TrigonometricFunc(new CotangentStub(),
                                                                new TangentStub(),
                                                                new CosecantStub(),
                                                                new SecantStub(),
                                                                new SinusStub());
       
        ICalculatable function = new MainFunc(logarithmFunc,  trigonometricFunc);
        Settings.getInstance()._mainFuncCvsName = "1lvl.cvs";
            for (int i=0; i < 10; i+=1)
            {
                System.out.printf("f(%d) = %f\n", i, function.Calculate(GetX(i)));
            }
    }
    
   
    
    @Test
    public void TestSecondLevelStubs() 
    {
        ICalculatable logarithm10Base = new Logarithm10Base(new NaturalLogarithmStub());        
        ICalculatable logarithm5Base = new Logarithm5Base(new NaturalLogarithmStub());        
        ICalculatable logarithm3Base = new Logarithm3Base(new NaturalLogarithmStub());           
        ICalculatable logarithm2Base = new Logarithm3Base(new NaturalLogarithmStub());         
        ICalculatable naturalLogarithm = new NaturalLogarithm();
        ICalculatable logarithmFunc = new LogarithmFunc(logarithm10Base,
                                                        logarithm5Base,
                                                        logarithm3Base,
                                                        logarithm2Base,
                                                        naturalLogarithm);
        ICalculatable cotangent = new Cotangent();
        ICalculatable tangent = new Tangent(cotangent);
        ICalculatable cosecant = new Cosecant(cotangent);
        ICalculatable secant = new Secant(cotangent);
        ICalculatable sinus = new Sinus(cotangent);
        ICalculatable trigonometricFunc = new TrigonometricFunc(cotangent,
                                                                tangent,
                                                                cosecant,
                                                                secant,
                                                                sinus);
        ICalculatable function = new MainFunc(logarithmFunc,  trigonometricFunc);
        Settings.getInstance()._mainFuncCvsName = "2lvl.cvs";
            for (int i=0; i < 10; i+=1)
            {
                System.out.printf("f(%d) = %f\n", i, function.Calculate(GetX(i)));
            }
    }
    
     @Test
    public void TestThirdLevelStubs() 
    {
        ICalculatable logarithm10Base = new Logarithm10Base();        
        ICalculatable logarithm5Base = new Logarithm5Base();        
        ICalculatable logarithm3Base = new Logarithm3Base();           
        ICalculatable logarithm2Base = new Logarithm3Base();         
        ICalculatable naturalLogarithm = new NaturalLogarithm();
        ICalculatable logarithmFunc = new LogarithmFunc(logarithm10Base,
                                                        logarithm5Base,
                                                        logarithm3Base,
                                                        logarithm2Base,
                                                        naturalLogarithm);
        ICalculatable cotangent = new Cotangent();
        ICalculatable tangent = new Tangent();
        ICalculatable cosecant = new Cosecant();
        ICalculatable secant = new Secant();
        ICalculatable sinus = new Sinus();
        ICalculatable trigonometricFunc = new TrigonometricFunc(cotangent,
                                                                tangent,
                                                                cosecant,
                                                                secant,
                                                                sinus);
        ICalculatable function =  new MainFunc(logarithmFunc,  trigonometricFunc);
        Settings.getInstance()._mainFuncCvsName = "3lvl.cvs";
            for (int i=0; i < 10; i+=1)
            {
                System.out.printf("f(%d) = %f\n", i, function.Calculate(GetX(i)));
            }
    }
    
     
}
