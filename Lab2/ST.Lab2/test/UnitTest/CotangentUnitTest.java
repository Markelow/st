package UnitTest;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Functions.Base.Cotangent;
import Infrastructure.Settings;

public class CotangentUnitTest {
    
    private final double tolerance = 0.00000000001;
    private static Cotangent _cotangent;
    
    public CotangentUnitTest() 
    {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        _cotangent = new Cotangent();;
    }

     @Test
     public void ZeroTest() 
     {
         assertEquals(Math.cos(0)/Math.sin(0),_cotangent.Calculate(0), tolerance);
     }
     
     @Test
     public void UnityTest() 
     {
         assertEquals(Math.cos(1)/Math.sin(1),_cotangent.Calculate(1), tolerance);
     }
     
     @Test
     public void NegativeTest() 
     {
         assertEquals(Math.cos(-2)/Math.sin(-2),_cotangent.Calculate(-2), tolerance);
     }
     
     @Test
     public void Positive3Test() 
     {
         assertEquals(Math.cos(3)/Math.sin(3), _cotangent.Calculate(3), tolerance);
     }
    
     
     @Test
     public void Positive9Test() 
     {
         assertEquals(Math.cos(9)/Math.sin(9),_cotangent.Calculate(9), tolerance);
     }
}
