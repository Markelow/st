package UnitTest;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Functions.Base.NaturalLogarithm;
import Infrastructure.Settings;

public class NaturalLogTest {
    
    private final double torerance = 1E-7;
    private static NaturalLogarithm _naturalLog;
    
    public NaturalLogTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        _naturalLog = new NaturalLogarithm();
    }

     @Test
    public void TestE() 
    {
        assertEquals(Math.log(Math.E), _naturalLog.Calculate(Math.E), torerance);
    } 
    
     @Test
    public void Test1() 
    {
        assertEquals(Math.log(1), _naturalLog.Calculate(1), torerance);
    }
    
    @Test
    public void TestNegative() 
    {
        assertEquals(Math.log(-1), _naturalLog.Calculate(-1), torerance);
    }
    
    @Test
    public void TestZero() 
    {
        assertEquals(Math.log(0), _naturalLog.Calculate(0), torerance);
    }
    
    @Test
    public void Test120() 
    {
        assertEquals(Math.log(120), _naturalLog.Calculate(120), torerance);
    }
}
