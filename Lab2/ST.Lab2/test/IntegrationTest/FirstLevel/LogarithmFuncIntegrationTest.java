/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IntegrationTest.FirstLevel;

import Functions.ICalculatable;
import Functions.LogarithmFunc;
import Functions.MainFunc;
import Functions.TrigonometricFunc;
import org.junit.BeforeClass;
import org.junit.Test;
import Stubs.Logarithm.*;
import Stubs.Trigonometric.*;
import static org.junit.Assert.*;

/**
 *
 * @author Markeli
 */
public class LogarithmFuncIntegrationTest {
    
    private final double _tolerance = 0.0001;
    private static MainFunc _mainFunc;
            
    public LogarithmFuncIntegrationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        ICalculatable logarithmFunc = new LogarithmFunc(new Logarithm10BaseStub(),
                                                        new Logarithm5BaseStub(),
                                                        new Logarithm3BaseStub(),
                                                        new Logarithm2BaseStub(),
                                                        new NaturalLogarithmStub());
        _mainFunc = new MainFunc(logarithmFunc, new TrigonometricFuncStub() );
    }

    
    @Test
    public void TestTrigonometricPositiveBranchHighLeft() 
    {
        assertEquals("4.71",  -3.9453E23,_mainFunc.Calculate(4.71),1E20);
    }
    
     @Test
    public void TestTrigonometricPositiveBranchDownLeft() 
    {
        assertEquals("5.39",  14.9442,_mainFunc.Calculate(5.39),_tolerance);
    } 
    
    @Test
    public void TestTrigonometricPosotiveBranchMiddle() 
    {
        assertEquals("6.28",  1.04454E-15, _mainFunc.Calculate(6.28),_tolerance);
    }
    @Test
    public void TestTrigonometricPositiveBranchDownRight() 
    {
        assertEquals("7.18", 15.8398, _mainFunc.Calculate(7.18),_tolerance);
    } 
    @Test
    public void TestTrigonometricPositiveBranchHighRight() 
    {
        assertEquals("7.85",  3.9759E21, _mainFunc.Calculate(7.85),1E19);
    } 
    @Test
    public void TestTrigonometricNegativeBranchDownLeft()
    {
        assertEquals("7.9",  -1.07706E12,_mainFunc.Calculate(7.9),1E7);
    }
    @Test
    public void TestTrigonometricNegativeBranchHighLeft()
    {
        assertEquals("9",  -0.0113216, _mainFunc.Calculate(9),_tolerance);
    }
    @Test
    public void TestLogarithmBorder() 
    {
        assertEquals("9.00001",  0.997073,_mainFunc.Calculate(9.00001),_tolerance);
    }
    
     @Test
    public void TestLogarithmNotBorderValue() 
    {
        assertEquals("20",  0.393407,_mainFunc.Calculate(20),1.E-3);
    }
    
     @Test
    public void TesLogarithmtBigValue() 
    {
        assertEquals("13000",  0.0124431,_mainFunc.Calculate(13000), _tolerance);
    }
}
