package IntegrationTest.SecondLevel;

import Functions.Base.*;
import IntegrationTest.FirstLevel.*;
import Functions.ICalculatable;
import Functions.LogarithmFunc;
import Functions.Logarithmic.*;
import Functions.MainFunc;
import Functions.TrigonometricFunc;
import org.junit.BeforeClass;
import org.junit.Test;
import Stubs.Logarithm.*;
import Stubs.Trigonometric.*;
import static org.junit.Assert.*;

public class CotangentIntegrationTest {
    
    private final double _tolerance = 0.0001;
    private static MainFunc _mainFunc;
            
    public CotangentIntegrationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        
        ICalculatable logarithm10Base = new Logarithm10Base(new NaturalLogarithmStub());        
        ICalculatable logarithm5Base = new Logarithm5Base(new NaturalLogarithmStub());        
        ICalculatable logarithm3Base = new Logarithm3Base(new NaturalLogarithmStub());           
        ICalculatable logarithm2Base = new Logarithm3Base(new NaturalLogarithmStub());         
        ICalculatable naturalLogarithm = new NaturalLogarithm();
        ICalculatable logarithmFunc = new LogarithmFunc(logarithm10Base,
                                                        logarithm5Base,
                                                        logarithm3Base,
                                                        logarithm2Base,
                                                        naturalLogarithm);
        ICalculatable cotangnet = new Cotangent();
        ICalculatable trigonometricFunc = new TrigonometricFunc(cotangnet,
                                                                new TangentStub(),
                                                                new CosecantStub(),
                                                                new SecantStub(),
                                                                new SinusStub());
        _mainFunc = new MainFunc(logarithmFunc,  trigonometricFunc);
    }

    
    @Test
    public void TestTrigonometricPositiveBranchHighLeft() 
    {
        assertEquals("4.71",  -3.9453E23,_mainFunc.Calculate(4.71),1E22);
    }
    
     @Test
    public void TestTrigonometricPositiveBranchDownLeft() 
    {
        assertEquals("5.39",  14.9442,_mainFunc.Calculate(5.39),1.E-2);
    } 
    
    @Test
    public void TestTrigonometricPosotiveBranchMiddle() 
    {
        assertEquals("6.28",  1.04454E-15, _mainFunc.Calculate(6.28),_tolerance);
    }
    @Test
    public void TestTrigonometricPositiveBranchDownRight() 
    {
        assertEquals("7.18", 15.8398, _mainFunc.Calculate(7.18), 1);
    } 
    @Test
    public void TestTrigonometricPositiveBranchHighRight() 
    {
        assertEquals("7.85",  3.9759E21, _mainFunc.Calculate(7.85),1E19);
    } 
    @Test
    public void TestTrigonometricNegativeBranchDownLeft()
    {
        assertEquals("7.9",  -1.07706E12,_mainFunc.Calculate(7.9),1E10);
    }
    @Test
    public void TestTrigonometricNegativeBranchHighLeft()
    {
        assertEquals("9",  -0.0113216, _mainFunc.Calculate(9),1E21);
    }
    @Test
    public void TestLogarithmBorder() 
    {
        assertEquals("9.00001",  0.997073,_mainFunc.Calculate(9.00001),1);
    }
    
     @Test
    public void TestLogarithmNotBorderValue() 
    {
        assertEquals("20",  0.393407,_mainFunc.Calculate(20),1E-1);
    }
    
     @Test
    public void TesLogarithmtBigValue() 
    {
        assertEquals("13000",  0.0124431,_mainFunc.Calculate(13000), 1E-1);
    }
}
