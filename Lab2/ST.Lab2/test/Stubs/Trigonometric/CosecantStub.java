package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class CosecantStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public CosecantStub()
    {
        table = new HashMap();
        table.put(9.00001, 2.42654);
        table.put(20.0, 1.09535);
        table.put(13000.0, 11.1757);
        table.put(4.71, -1.0);
        table.put(5.39, -1.2835);
        table.put(6.28, -313.94);
        table.put(7.18, 1.27985);
        table.put(7.85, 1.00001);
        table.put(7.9, 1.00106);
        table.put(9.0, 2.42648);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
