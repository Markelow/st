package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class CotangentStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public CotangentStub()
    {
        table = new HashMap();
        table.put(9.00001, -2.2109);
        table.put(20.0, 0.44699);
        table.put(13000.0, -1.4026);
        table.put(4.71, 0.00238);
        table.put(5.39, -0.8047);
        table.put(6.28, -313.94);
        table.put(7.18, 0.79875);
        table.put(7.85, 0.0039816);
        table.put(7.9,  -0.0460);
        table.put(9.0,  -2.2108);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
