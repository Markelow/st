package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class TrigonometricFuncStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public TrigonometricFuncStub()
    {
        table = new HashMap();
        table.put(4.71, -3.94533E23);
        table.put(5.39, 14.9442);
        table.put(6.28, -1.04454E-15);
        table.put(7.18, 15.8398);
        table.put(7.85, 3.97592E21);
        table.put(7.9, -1.07706E12);
        table.put(9.0, -0.0113216);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get((Double)x);
    }
    
}
