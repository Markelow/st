package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class SecantStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public SecantStub()
    {
        table = new HashMap();
        table.put(9.00001, -1.0975);
        table.put(20.0, 2.45048);
        table.put(13000.0, 1.00402);
        table.put(4.71, -418.58);
        table.put(5.39, 1.59507);
        table.put(6.28, 1.00001);
        table.put(7.18, 1.60230);
        table.put(7.85, 251.154);
        table.put(7.9, -21.738);
        table.put(9.0, -0.4523);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
