package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class TangentStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public TangentStub()
    {
        table = new HashMap();
        table.put(9.00001, -0.4523);
        table.put(20.0, 2.23716);
        table.put(13000.0, -0.7129);
        table.put(4.71, 418.588);
        table.put(5.39, -1.2426);
        table.put(6.28, -0.0031);
        table.put(7.18, 1.25195);
        table.put(7.85, 251.1518441);
        table.put(7.9, -21.715);
        table.put(9.0, -0.4523);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
