package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.ICalculatable;

public class SinusStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public SinusStub()
    {
        table = new HashMap();
        table.put(9.00001, 0.412109);
        table.put(20.0, 0.912945);
        table.put(13000.0, -0.58051);
        table.put(4.71, -0.99997);
        table.put(5.39, -0.77907);
        table.put(6.28, -0.00318);
        table.put(7.18, 0.78343);
        table.put(7.85, 0.999992);
        table.put(7.9, 0.998941);
        table.put(9.0, 0.412118);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
