package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class Logarithm5BaseStub implements ICalculatable 
{
    private  HashMap<Double, Double> table;
    
    public Logarithm5BaseStub()
    {
        table = new HashMap();
        table.put(9.00001, 1.36521);
        table.put(20.0, 1.861);
        table.put(13000.0, 5.886);
        table.put(4.71, 0.9629);
        table.put(5.39, 1.074);
        table.put(6.28, 1.142);
        table.put(7.18, 1.225);
        table.put(7.85, 1.28);
        table.put(7.9,  1.283);
        table.put(9.0,  1.37);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
