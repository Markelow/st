package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class Logarithm10BaseStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public Logarithm10BaseStub()
    {
        table = new HashMap();
        table.put(9.00001, 0.95424);
        table.put(20.0, 1.301);
        table.put(13000.0, 4.114);
        table.put(4.71, 0.673);
        table.put(5.39, 0.7316);
        table.put(6.28, 0.798);
        table.put(7.18, 0.8561);
        table.put(7.85, 0.8949);
        table.put(7.9,  0.898);
        table.put(9.0,  0.9542);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
