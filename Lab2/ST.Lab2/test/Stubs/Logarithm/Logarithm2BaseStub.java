package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class Logarithm2BaseStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public Logarithm2BaseStub()
    {
        table = new HashMap();
        table.put(9.00001, 3.16993);
        table.put(20.0, 4.322);
        table.put(13000.0, 13.67);
        table.put(4.71, 2.236);
        table.put(5.39, 2.43);
        table.put(6.28, 2.651);
        table.put(7.18, 2.844);
        table.put(7.85, 2.973);
        table.put(7.9,  2.98);
        table.put(9.0,  3.17);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
