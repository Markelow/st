package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class Logarithm3BaseStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public Logarithm3BaseStub()
    {
        table = new HashMap();
        table.put(9.00001, 2.0);
        table.put(20.0, 2.727);
        table.put(13000.0, 8.622);
        table.put(4.71, 1.411);
        table.put(5.39, 1.533);
        table.put(6.28, 1.672);
        table.put(7.18, 1.794);
        table.put(7.85, 1.876);
        table.put(7.9,  1.88);
        table.put(9.0,  2.0);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
