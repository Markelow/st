package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class LogarithmFuncStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public LogarithmFuncStub()
    {
        table = new HashMap();
        table.put(9.00001, 0.997073);
        table.put(20.0, 0.393407);
        table.put(13000.0, 0.0124431);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get((Double)x);
    }
    
}
