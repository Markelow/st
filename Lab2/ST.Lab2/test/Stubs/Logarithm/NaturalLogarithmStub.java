package Stubs.Logarithm;

import java.util.HashMap;
import Functions.ICalculatable;

public class NaturalLogarithmStub implements ICalculatable 
{
    private HashMap<Double, Double> table;
    
    public NaturalLogarithmStub()
    {
        table = new HashMap();
        table.put(9.00001, 2.19723);
        table.put(20.0, 2.99572);
        table.put(13000.0, 9.47270);
        table.put(4.71, 1.54969);
        table.put(5.39, 1.68454);
        table.put(6.28, 1.83737);
        table.put(7.18, 1.97130);
        table.put(7.85, 2.06051);
        table.put(7.9,  2.06686);
        table.put(9.0,  2.19722);
        table.put(10.0, 2.30225);
        table.put(5.0, 1.60943);
        table.put(3.0, 1.09861);
        table.put(2.0, 0.693147);
    }
    
    @Override
    public double Calculate(double x) 
    {
        System.out.println(x);
        return table.get((Double)x);
    }
    
}
