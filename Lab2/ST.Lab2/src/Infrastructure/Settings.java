/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Infrastructure;
/**
 *
 * @author Markeli
 */
public class Settings 
{
    public final double DefaultTolerance = 1e-10;
    
    //Singletone
    private static Settings _instance;
    
    public static Settings getInstance()
    {
        if (_instance == null)
        {
            _instance = new Settings();
        }
        return _instance;
    }
   
    private Settings()
    {
        _tolerance = DefaultTolerance;
        _mainFuncCvsName = "mainFunc.cvs";
        _logarithmFuncCvsName = "logFunc.cvs";
        _trigonometricFuncCvsName = "trigFunc.cvs";
        _cotangentCvsName = "cot.cvs";
        _naturalLogarithmCvsName = "ln.cvs";
        _logarithm10BaseCvsName = "log10.cvs";
        _logarithm5BaseCvsName = "log5.cvs";
        _logarithm3BaseCvsName = "log3.cvs";
        _logarithm2BaseCvsName = "log2.cvs";
        _cosecantCvsName = "csc.cvs";
        _secantCvsName = "sec.cvs";
        _sinusCvsName = "sin.cvs";
        _tangentCvsName = "tan.cvs";
    }
    
    private double _tolerance;    
    public  String _mainFuncCvsName;
    private final String _logarithmFuncCvsName;
    private final String _trigonometricFuncCvsName;
    private final String _cotangentCvsName;
    private final String _naturalLogarithmCvsName;
    private final String _logarithm10BaseCvsName;
    private final String _logarithm5BaseCvsName;
    private final String _logarithm3BaseCvsName;
    private final String _logarithm2BaseCvsName;
    private final String _cosecantCvsName;
    private final String _secantCvsName;
    private final String _sinusCvsName;
    private final String _tangentCvsName;
    private boolean _needSaveAll;
    private boolean _needSaveMainFunc;
    private boolean _needSaveLogarithmFunc;
    private boolean _needSaveTrigonometricFunc;
    private boolean _needSaveNaturalLogarithm;
    private boolean _needSaveLogarithm10Base;
    private boolean _needSaveLogarithm5Base;
    private boolean _needSaveLogarithm3Base;
    private boolean _needSaveLogarithm2Base;
    private boolean _needSaveCotangent;
    private boolean _needSaveCosecant;
    private boolean _needSaveSecant;
    private boolean _needSaveSinus;
    private boolean _needSaveTangent;
    
    public double getTolerance()
    {
        return _tolerance;
    }
    
    public void setTolerance(double value)
    {
        _tolerance = value;
    }
        
    /**
     * @return the _mainFuncCvsName
     */
    public String getMainFuncCvsName() {
        return _mainFuncCvsName;
    }

    /**
     * @return the _logarithmFuncCvsName
     */
    public String getLogarithmFuncCvsName() {
        return _logarithmFuncCvsName;
    }

    /**
     * @return the _trigonometricFuncCvsName
     */
    public String getTrigonometricFuncCvsName() {
        return _trigonometricFuncCvsName;
    }

    /**
     * @return the _cotangentCvsName
     */
    public String getCotangentCvsName() {
        return _cotangentCvsName;
    }

    /**
     * @return the _naturalLogarithmCvsName
     */
    public String getNaturalLogarithmCvsName() {
        return _naturalLogarithmCvsName;
    }

    /**
     * @return the _logarithm10BaseCvsName
     */
    public String getLogarithm10BaseCvsName() {
        return _logarithm10BaseCvsName;
    }

    /**
     * @return the _logarithm5BaseCvsName
     */
    public String getLogarithm5BaseCvsName() {
        return _logarithm5BaseCvsName;
    }

    /**
     * @return the _logarithm3BaseCvsName
     */
    public String getLogarithm3BaseCvsName() {
        return _logarithm3BaseCvsName;
    }

    /**
     * @return the _logarithm2BaseCvsName
     */
    public String getLogarithm2BaseCvsName() {
        return _logarithm2BaseCvsName;
    }

    /**
     * @return the _cosecantCvsName
     */
    public String getCosecantCvsName() {
        return _cosecantCvsName;
    }

    /**
     * @return the _secantCvsName
     */
    public String getSecantCvsName() {
        return _secantCvsName;
    }

    /**
     * @return the _sinusCvsName
     */
    public String getSinusCvsName() {
        return _sinusCvsName;
    }

    /**
     * @return the _tangentCvsName
     */
    public String getTangentCvsName() {
        return _tangentCvsName;
    }

    /**
     * @return the _neadSaveMainFunc
     */
    public boolean isNeadSaveMainFunc() {
        return _needSaveMainFunc;
    }

    /**
     * @param _neadSaveMainFunc the _neadSaveMainFunc to set
     */
    public void setNeadSaveMainFunc(boolean _neadSaveMainFunc) {
        this._needSaveMainFunc = _neadSaveMainFunc;
    }

    /**
     * @return the _needSaveLogFunc
     */
    public boolean isNeedSaveLogarithmFunc() {
        return _needSaveLogarithmFunc;
    }

    /**
     * @return the _needSaveTrigFunc
     */
    public boolean isNeedSaveTrigonometricFunc() {
        return _needSaveTrigonometricFunc;
    }

    /**
     * @return the _needSaveLn
     */
    public boolean isNeedSaveNaturalLogarithm() {
        return _needSaveNaturalLogarithm;
    }

    /**
     * @return the _needSaveLog10
     */
    public boolean isNeedSaveLogarithm10Base() {
        return _needSaveLogarithm10Base;
    }

    /**
     * @return the _needSaveLog5
     */
    public boolean isNeedSaveLogarithm5Base() {
        return _needSaveLogarithm5Base;
    }

    /**
     * @return the _needSaveLog3
     */
    public boolean isNeedSaveLogarithm3Base() {
        return _needSaveLogarithm3Base;
    }

    /**
     * @return the _needSaveLog2
     */
    public boolean isNeedSaveLogarithm2Base() {
        return _needSaveLogarithm2Base;
    }

    /**
     * @return the _needSaveCot
     */
    public boolean isNeedSaveCotangent() {
        return _needSaveCotangent;
    }

    /**
     * @return the _needSaveCsc
     */
    public boolean isNeedSaveCosecant() {
        return _needSaveCosecant;
    }

    /**
     * @return the _needSaveSec
     */
    public boolean isNeedSaveSecant() {
        return _needSaveSecant;
    }

    /**
     * @return the _needSaveSin
     */
    public boolean isNeedSaveSinus() {
        return _needSaveSinus;
    }

    /**
     * @return the _needSaveTan
     */
    public boolean isNeedSaveTangent() {
        return _needSaveTangent;
    }

    /**
     * @return the _needSaveAll
     */
    public boolean isNeedSaveAll() {
        return _needSaveAll;
    }

    /**
     * @param _needSaveAll the _needSaveAll to set
     */
    public void setNeedSaveAll(boolean _needSaveAll) 
    {
        this._needSaveAll = _needSaveAll;
        if (_needSaveAll)
        {
            _needSaveTangent = true;
            _needSaveSinus = true;
            _needSaveSecant = true;
            _needSaveCosecant = true;
            _needSaveCotangent = true;
            _needSaveLogarithm5Base = true;
            _needSaveLogarithm3Base = true;
            _needSaveLogarithm2Base = true;
            _needSaveLogarithm10Base = true;
            _needSaveNaturalLogarithm = true;
            _needSaveMainFunc = true;
            _needSaveLogarithmFunc = true;
            _needSaveTrigonometricFunc = true;
        }
    }

    /**
     * @param _needSaveLogFunc the _needSaveLogFunc to set
     */
    public void setNeedSaveLogarithmFunc(boolean _needSaveLogFunc) {
        this._needSaveLogarithmFunc = _needSaveLogFunc;
    }

    /**
     * @param _needSaveTrigFunc the _needSaveTrigFunc to set
     */
    public void setNeedSaveTrigonometricFunc(boolean _needSaveTrigFunc) {
        this._needSaveTrigonometricFunc = _needSaveTrigFunc;
    }

    /**
     * @param _needSaveLn the _needSaveLn to set
     */
    public void setNeedSaveNaturalLogarithm(boolean _needSaveLn) {
        this._needSaveNaturalLogarithm = _needSaveLn;
    }

    /**
     * @param _needSaveLog10 the _needSaveLog10 to set
     */
    public void setNeedSaveLogarithm10Base(boolean _needSaveLog10) {
        this._needSaveLogarithm10Base = _needSaveLog10;
    }

    /**
     * @param _needSaveLog5 the _needSaveLog5 to set
     */
    public void setNeedSaveLogarithm5Base(boolean _needSaveLog5) {
        this._needSaveLogarithm5Base = _needSaveLog5;
    }

    /**
     * @param _needSaveLog3 the _needSaveLog3 to set
     */
    public void setNeedSaveLogarithm3Base(boolean _needSaveLog3) {
        this._needSaveLogarithm3Base = _needSaveLog3;
    }

    /**
     * @param _needSaveLog2 the _needSaveLog2 to set
     */
    public void setNeedSaveLogarithm2Base(boolean _needSaveLog2) {
        this._needSaveLogarithm2Base = _needSaveLog2;
    }

    /**
     * @param _needSaveCot the _needSaveCot to set
     */
    public void setNeedSaveCotangent(boolean _needSaveCot) {
        this._needSaveCotangent = _needSaveCot;
    }

    /**
     * @param _needSaveCsc the _needSaveCsc to set
     */
    public void setNeedSaveCosecant(boolean _needSaveCsc) {
        this._needSaveCosecant = _needSaveCsc;
    }

    /**
     * @param _needSaveSec the _needSaveSec to set
     */
    public void setNeedSaveSecant(boolean _needSaveSec) {
        this._needSaveSecant = _needSaveSec;
    }

    /**
     * @param _needSaveSin the _needSaveSin to set
     */
    public void setNeedSaveSinus(boolean _needSaveSin) {
        this._needSaveSinus = _needSaveSin;
    }

    /**
     * @param _needSaveTan the _needSaveTan to set
     */
    public void setNeedSaveTangent(boolean _needSaveTan) {
        this._needSaveTangent = _needSaveTan;
    }
}
