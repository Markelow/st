package Functions.Trigonometric;

import Functions.Base.Cotangent;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Tangent implements ICalculatable
{
    private final ICalculatable _cotangent;
    
    public Tangent()
    {
        _cotangent = new Cotangent();
    }
    
    public Tangent(ICalculatable cotangent)
    {
        _cotangent = cotangent;
    }
    
    @Override
    public double Calculate(double x)
    {
        double result = 1/_cotangent.Calculate(x);
        if (Settings.getInstance().isNeedSaveTangent())
        {
            CvsWriter.Wirte(Settings.getInstance().getTangentCvsName(), x, result);
        }
        return result;
    }
    
}
