/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions.Trigonometric;

import Functions.Base.Cotangent;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Cosecant  implements ICalculatable
{
    private final ICalculatable _cotangent;
    
    public Cosecant()
    {
        _cotangent = new Cotangent();
    }
    
    public Cosecant(ICalculatable cotangent)
    {
        _cotangent = cotangent;
    }
    
    @Override
    public double Calculate(double x)
    {
        double result = GetResultSign(x) * Math.pow(Math.pow(_cotangent.Calculate(x), 2) + 1, 0.5);
        if (Settings.getInstance().isNeedSaveCosecant())
        {
            CvsWriter.Wirte(Settings.getInstance().getCosecantCvsName(), x, result);
        }
        return result;
    }
    
    private int GetResultSign(double x)
    {
        //Знак аргумента
        int xSign = (x < 0) ? -1: +1; 
        //Номер периода, в который попадает аргумент
        int periodNumber = (int)(Math.abs(x) / (Math.PI)) + 1;
        
        //Если номер периода нечетный, то scs положителен
        int resultSign = (periodNumber % 2 == 1) ? +1 : -1;        
        
        return resultSign*xSign;
    }
    
}
