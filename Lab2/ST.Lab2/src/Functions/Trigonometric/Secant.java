package Functions.Trigonometric;

import Functions.Base.Cotangent;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Secant implements ICalculatable
{
    private final ICalculatable _cotangent;
    
    public Secant()
    {
        _cotangent = new Cotangent();
    }
    
    public Secant(ICalculatable cotangent)
    {
        _cotangent = cotangent;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = GetResultSign(x)*Math.pow(Math.pow(1/_cotangent.Calculate(x), 2) + 1, 0.5);
        if (Settings.getInstance().isNeedSaveSecant())
        {
            CvsWriter.Wirte(Settings.getInstance().getSecantCvsName(), x, result);
        }
        return result;
    }
    
    private int GetResultSign(double x)
    {
        //Смещаем на pi/2 вправо, чтобы воспользоваться методом для косеконса
        x += Math.PI/2;
        //Знак аргумента
        int xSign = (x < 0) ? -1: +1; 
        //Номер периода, в который попадает аргумент
        int periodNumber = (int)(Math.abs(x) / (Math.PI)) + 1;
        
        //Если номер периода нечетный, то scs положителен
        int resultSign = (periodNumber % 2 == 1) ? +1 : -1;        
        
        return resultSign*xSign;
    }
}
