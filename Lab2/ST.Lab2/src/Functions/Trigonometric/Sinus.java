package Functions.Trigonometric;

import Functions.Base.Cotangent;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Sinus implements ICalculatable
{
    private final ICalculatable _cotangent;
    
    public Sinus()
    {
        _cotangent = new Cotangent();
    }
    
    public Sinus(ICalculatable cotangent)
    {
        _cotangent = cotangent;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = Math.pow(1/(Math.pow(_cotangent.Calculate(x), 2) + 1), 0.5);
        //Из формулы потеряли знак, нужно его определить.
        result *= GetResultSign(x);
        if (Settings.getInstance().isNeedSaveSinus())
        {
            CvsWriter.Wirte(Settings.getInstance().getSinusCvsName(), x, result);
        }
        return result;
    }
    
    private int GetResultSign(double x)
    {
        //Знак аргумента
        int xSign = (x < 0) ? -1: +1; 
        //Номер периода, в который попадает аргумент
        int periodNumber = (int)(Math.abs(x) / ( 2* Math.PI));
        
        //Середина этого периода с учетом знакак аргумента
        double periodMiddle = xSign * Math.PI * (2*periodNumber+1);
        return (x <= periodMiddle) ? +1: -1;
    }
    
}
