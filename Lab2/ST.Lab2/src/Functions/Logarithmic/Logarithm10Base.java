package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Logarithm10Base implements ICalculatable
{
    private final ICalculatable _naturalLog;
    
    public Logarithm10Base()
    {
        _naturalLog = new NaturalLogarithm();
    }
    
    public Logarithm10Base(ICalculatable naturalLog)
    {
        _naturalLog = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = _naturalLog.Calculate(x) / _naturalLog.Calculate(10);
        if (Settings.getInstance().isNeedSaveLogarithm10Base())
        {
            CvsWriter.Wirte(Settings.getInstance().getLogarithm10BaseCvsName(), x, result);
        }
        return result;
    }    
}
