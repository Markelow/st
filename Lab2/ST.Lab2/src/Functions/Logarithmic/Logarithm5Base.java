package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Logarithm5Base implements ICalculatable
{
    private final ICalculatable _naturalLog;
    
    public Logarithm5Base()
    {
        _naturalLog = new NaturalLogarithm();
    }
    
    public Logarithm5Base(ICalculatable naturalLog)
    {
        _naturalLog = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = _naturalLog.Calculate(x) / _naturalLog.Calculate(5);
        if (Settings.getInstance().isNeedSaveLogarithm5Base())
        {
            CvsWriter.Wirte(Settings.getInstance().getLogarithm5BaseCvsName(), x, result);
        }
        return result;
    }    
}
