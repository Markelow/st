package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Logarithm2Base implements ICalculatable
{
    private final ICalculatable _naturalLog;
    
    public Logarithm2Base()
    {
        _naturalLog = new NaturalLogarithm();
    }
    
    public Logarithm2Base(ICalculatable naturalLog)
    {
        _naturalLog = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = _naturalLog.Calculate(x) / _naturalLog.Calculate(2);
        if (Settings.getInstance().isNeedSaveLogarithm2Base())
        {
            CvsWriter.Wirte(Settings.getInstance().getLogarithm2BaseCvsName(), x, result);
        }
        return result;
    }    
}
