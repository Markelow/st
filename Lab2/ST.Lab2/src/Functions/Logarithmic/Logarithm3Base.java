package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class Logarithm3Base implements ICalculatable
{
    private final ICalculatable _naturalLog;
    
    public Logarithm3Base()
    {
        _naturalLog = new NaturalLogarithm();
    }
    
    public Logarithm3Base(ICalculatable naturalLog)
    {
        _naturalLog = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = _naturalLog.Calculate(x) / _naturalLog.Calculate(3);
        if (Settings.getInstance().isNeedSaveLogarithm3Base())
        {
            CvsWriter.Wirte(Settings.getInstance().getLogarithm3BaseCvsName(), x, result);
        }
        return result;
    }    
}
