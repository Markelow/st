package Functions;

import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class MainFunc implements ICalculatable
{
    private final ICalculatable _logarithmFunc;
    private final ICalculatable _trigonometricFunc;
    
    
    public MainFunc()
    {
        _logarithmFunc = new LogarithmFunc();
        _trigonometricFunc = new TrigonometricFunc();
    }
    
    public MainFunc(ICalculatable logarithmFunc, ICalculatable trigonometricFunc)
    {
        _logarithmFunc = logarithmFunc;
        _trigonometricFunc = trigonometricFunc;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = (x > 9) ? _logarithmFunc.Calculate(x):_trigonometricFunc.Calculate(x) ;
        if (Settings.getInstance().isNeadSaveMainFunc())
        {
            CvsWriter.Wirte(Settings.getInstance().getMainFuncCvsName(), x, result);
        }
        return result;
    }
    
}
