    package Functions;

import Functions.Base.Cotangent;
import Functions.ICalculatable;
import Functions.Trigonometric.*;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;


public class TrigonometricFunc implements ICalculatable
{
    private final ICalculatable _cotangent;
    private final ICalculatable _tangent;
    private final ICalculatable _cosecant;
    private final ICalculatable _secant;
    private final ICalculatable _sinus;
    
    public TrigonometricFunc()
    {
        _cotangent = new Cotangent();
        _tangent = new Tangent();
        _cosecant = new Cosecant();
        _secant = new Secant();
        _sinus = new Sinus();
    }
    
    public TrigonometricFunc(ICalculatable cotangent, 
                             ICalculatable tangent,
                             ICalculatable cosecant,
                             ICalculatable secant,
                             ICalculatable sinus)
    {
        _cotangent = cotangent;
        _tangent = tangent;
        _cosecant = cosecant;
        _secant = secant;
        _sinus = sinus;
    }


    @Override
    public double Calculate(double x) 
    {
        double cotX = _cotangent.Calculate(x);
        double tanX = _tangent.Calculate(x);
        double cscX = _cosecant.Calculate(x);
        double secX = _secant.Calculate(x);
        double sinX = _sinus.Calculate(x);
        double result =  ((Math.pow(((cotX * tanX) * sinX)*(Math.pow(secX, 2)), 3)) * ((Math.pow(secX/cscX, 2)) * tanX));
        if (Settings.getInstance().isNeedSaveTrigonometricFunc())
        {
            CvsWriter.Wirte(Settings.getInstance().getTrigonometricFuncCvsName(), x, result);
        }
        return result;
    }
    
}
