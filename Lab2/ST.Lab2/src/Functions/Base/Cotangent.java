package Functions.Base;

import Functions.*;
import Infrastructure.*;

public class Cotangent implements ICalculatable
{
    @Override
    public double Calculate(double x)
    {
        double cos = Cos(x);
        double sin = Sin(x);
        double result = cos/sin; 
        if (Settings.getInstance().isNeedSaveCotangent())
        {
            CvsWriter.Wirte(Settings.getInstance().getCotangentCvsName(), x, result);
        }
        return  result;
    }    
    
    private double Sin(double x)
    {
        double tolerance = Settings.getInstance().getTolerance();
        double value, previousValue;
        double termFactor = (-1) * Math.pow(x, 2);
        double term = x;
        int n = 3;
        value = term;
        do   
        {
            previousValue = value;
            term  *= termFactor / (n *(n-1));
            value += term;
            n +=2;
        }
        while (tolerance <= Math.abs(value - previousValue));
        return value;
    }
    
    private double Cos(double x)
    {
        double tolerance = Settings.getInstance().getTolerance();
        double value, previousValue;
        double termFactor = (-1) * Math.pow(x, 2);
        double term = 1;
        int n = 2;
        value = term;
        do   
        {
            previousValue = value;
            term  *= termFactor / (n *(n-1));
            value += term;
            n +=2;
        }
        while (tolerance <= Math.abs(value - previousValue));
        return value;
    }
}
