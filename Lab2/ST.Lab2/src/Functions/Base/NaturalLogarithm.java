package Functions.Base;

import Functions.ICalculatable;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class NaturalLogarithm implements ICalculatable
{
    @Override
    public double Calculate(double argument) 
    {
        if (argument == 0) { return Double.NEGATIVE_INFINITY; }
        if (argument < 0) { return Double.NaN; }
        double x = (argument )/(argument - 1);
        double value, previousValue;
        double termFactor =  1/x;
        double term = 1/x;
        value = term;
        int n = 2;
        do
        {
            previousValue = value;
            term *= termFactor ;
            value += term / n;
            n++;
        }
        while (Settings.getInstance().getTolerance() <= Math.abs(value - previousValue));
        if (Settings.getInstance().isNeedSaveNaturalLogarithm())
        {
            CvsWriter.Wirte(Settings.getInstance().getNaturalLogarithmCvsName(), x, value);
        }
        return value;
    } 
}
