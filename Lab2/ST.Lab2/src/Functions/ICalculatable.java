package Functions;

/**
 *
 * @author Markeli
 */
public interface ICalculatable {
    
    double Calculate(double x);
    
}
