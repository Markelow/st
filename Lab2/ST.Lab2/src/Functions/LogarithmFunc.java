package Functions;

import Functions.Base.NaturalLogarithm;
import Functions.Logarithmic.*;
import Infrastructure.CvsWriter;
import Infrastructure.Settings;

public class LogarithmFunc implements ICalculatable
{
    private final ICalculatable _log10;
    private final ICalculatable _log5;
    private final ICalculatable _log3;
    private final ICalculatable _log2;
    private final ICalculatable _ln;
    
    
    public LogarithmFunc()
    {
        _log10 = new Logarithm10Base();
        _log5 = new Logarithm5Base();
        _log3 = new Logarithm3Base();
        _log2 = new Logarithm2Base();
        _ln = new NaturalLogarithm();
    }
    
    public LogarithmFunc(ICalculatable log10,
                         ICalculatable log5,
                         ICalculatable log3,
                         ICalculatable log2,
                         ICalculatable ln)
    {
        _log10 = log10;
        _log5 = log5;
        _log3 = log3;
        _log2 = log2;
        _ln = ln;
    }

    @Override
    public double Calculate(double x) 
    {
        double log_10x = _log10.Calculate(x);
        double log_5x = _log5.Calculate(x);
        double log_3x = _log3.Calculate(x);
        double log_2x = _log2.Calculate(x);
        double lnX = _ln.Calculate(x);
        double result = (((((log_10x + log_3x) + log_2x) / log_5x) / ((log_5x / lnX) * (Math.pow(lnX, 3)))) / (log_5x / log_3x));
        if (Settings.getInstance().isNeedSaveLogarithmFunc())
        {
            CvsWriter.Wirte(Settings.getInstance().getLogarithmFuncCvsName(), x, result);
        }
        return result;
    }
    
}
