import Functions.*;
import Infrastructure.Settings;
import java.io.File;

public class main {
    
    private final static String Usage = "Usage: appName startX endX step [all] [sin] [sec] [csc] [tan] [cot] [trig] [log] [ln] [log3] [log2] [log5] [log10] [main]"; 
    
    public static void main(String args[])
    {
        if (args.length < 3)
        {
            System.out.println(Usage);
            return;
        }
        try
        {
            double startX = Double.parseDouble(args[0]);
            double endX = Double.parseDouble(args[1]);
            if (startX > endX)
            {
                double temp = endX;
                endX = startX;
                startX = temp;
            }
            double step = Double.parseDouble(args[2]);
            if (step < 0)
            {
                throw new Exception("Step should be a positive number");
            }
            if (args.length >= 4)
            {
                PrepareFiles(args);
            }
            ICalculatable function = new MainFunc();
            for (double i=startX; i < endX; i+=step)
            {
                System.out.printf("f(%f) = %f\n", i, function.Calculate(i));
            }
        }
        catch (Exception ex)
        {
            System.err.println("Error: " + ex.getMessage());
        }
    } 
        
    private static void PrepareFiles(String args[])
    {
        File file;
        for (int i=3; i<args.length; ++i)
        {
            switch (args[i])
            {
                case "all":
                    Settings.getInstance().setNeedSaveAll(true);
                    file = new File(Settings.getInstance().getMainFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getTrigonometricFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getLogarithmFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getCosecantCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getSinusCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getLogarithm10BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getLogarithm5BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getLogarithm3BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getLogarithm2BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getNaturalLogarithmCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getTangentCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getSecantCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    file = new File(Settings.getInstance().getCotangentCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "main":
                    Settings.getInstance().setNeadSaveMainFunc(true);
                    file = new File(Settings.getInstance().getMainFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "trig":
                    Settings.getInstance().setNeedSaveTrigonometricFunc(true);
                    file = new File(Settings.getInstance().getTrigonometricFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "log":                    
                    Settings.getInstance().setNeedSaveLogarithmFunc(true);
                    file = new File(Settings.getInstance().getLogarithmFuncCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "csc":
                    Settings.getInstance().setNeedSaveCosecant(true);
                    file = new File(Settings.getInstance().getCosecantCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "sin":
                    Settings.getInstance().setNeedSaveSinus(true);
                    file = new File(Settings.getInstance().getSinusCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "cot":
                    Settings.getInstance().setNeedSaveCotangent(true);
                    file = new File(Settings.getInstance().getCotangentCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "sec":
                    Settings.getInstance().setNeedSaveSecant(true);
                    file = new File(Settings.getInstance().getSecantCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "tan":
                    Settings.getInstance().setNeedSaveTangent(true);
                    file = new File(Settings.getInstance().getTangentCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "ln":
                    Settings.getInstance().setNeedSaveNaturalLogarithm(true);
                    file = new File(Settings.getInstance().getNaturalLogarithmCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "log2":
                    Settings.getInstance().setNeedSaveLogarithm2Base(true);
                    file = new File(Settings.getInstance().getLogarithm2BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "log3":
                    Settings.getInstance().setNeedSaveLogarithm3Base(true);
                    file = new File(Settings.getInstance().getLogarithm3BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "log5":
                    Settings.getInstance().setNeedSaveLogarithm5Base(true);
                    file = new File(Settings.getInstance().getLogarithm5BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
                case "log10":
                    Settings.getInstance().setNeedSaveLogarithm10Base(true);
                    file = new File(Settings.getInstance().getLogarithm10BaseCvsName());
                    if (file.exists())
                    {
                        file.delete();
                    }
                    break;
            }
        }
    }
    
}
