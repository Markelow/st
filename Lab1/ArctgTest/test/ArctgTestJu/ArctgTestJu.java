package ArctgTestJu;

import arctgtest.Arctg;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ArctgTestJu 
{      
    Arctg arctg;
    
    public ArctgTestJu() 
    {
    }
    
    @Before 
    public void Init()
    {
        arctg = new Arctg(); 
    }
    
    @Test
    public void TestNegativeStraight() 
    {       
        assertEquals("-10", Math.atan(-10), arctg.Calculate(-10), arctg.Tolerance);
        assertEquals("-4.1", Math.atan(-4.1), arctg.Calculate(-4.1), arctg.Tolerance);
    }
    
    @Test
    public void TestNegativeCurve() 
    {       
        assertEquals("-4", Math.atan(-4), arctg.Calculate(-4), arctg.Tolerance);
        assertEquals("-1.2", Math.atan(-1.2), arctg.Calculate(-1.2), arctg.Tolerance);
    }
    
    @Test
    public void TestCentral() 
    {       
        assertEquals("-1.1", Math.atan(-1.1), arctg.Calculate(-1.1), arctg.Tolerance);
        assertEquals("0", Math.atan(0), arctg.Calculate(0), arctg.Tolerance);
        assertEquals("1.1", Math.atan(1.1), arctg.Calculate(1.1), arctg.Tolerance);
    }
    
    @Test
    public void TestPositiveCurve() 
    {       
        assertEquals("4", Math.atan(4), arctg.Calculate(4), arctg.Tolerance);
        assertEquals("1.2", Math.atan(1.2), arctg.Calculate(1.2), arctg.Tolerance);
    }
    
    @Test
    public void TestPositiveStraight() 
    {       
        assertEquals("10", Math.atan(10), arctg.Calculate(10), arctg.Tolerance);
        assertEquals("4.1", Math.atan(4.1), arctg.Calculate(4.1), arctg.Tolerance);
    }
}
