/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package arctgtest;

/**
 *
 * @author Markeli
 */
public class Arctg 
{
    public final double Tolerance = 0.00000001;
    
    
    public double Calculate(double x)
    {
        if (0 == x) { return 0;}
        double value = 0, previosValue;
        int k = 0;
        if (Math.abs(x) < 1)
        {
           do   
           {
               previosValue = value;
               value += CalculateLessOne(x, k);
               k++;
           }
           while (Tolerance <= Math.abs(value - previosValue));
        }
        else
        {
           value = Math.PI * Math.pow( x * x, 0.5)/( 2 * x);
           do   
           {
               previosValue = value;
               value -= CalculateMoreOne(x, k);
               k++;
           }
           while (Tolerance <= Math.abs(value - previosValue));
        }
        
        return value;
    }
        
    private double CalculateLessOne(double x, double k)
    {
        return Math.pow(-1, k) * Math.pow(x, 1+2*k)/(1+2*k);
    }
    
    private double CalculateMoreOne(double x, double k)
    {
        return Math.pow(-1, k) * Math.pow(x, -1-2*k)/(1+2*k);
    }
    
}
