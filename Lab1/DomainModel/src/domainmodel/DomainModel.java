/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domainmodel;

/**
 *
 * @author Markeli
 */
public class DomainModel 
{
    private Person Ford;
    private Person Prosser;
    private Dream prossersDream;
    private Filth filths;
    private SeatingInPuddleStatus prossersStatus;
    private Fillings prossersFiilngs;
    private Puddle puddle;
    
    public DomainModel()
    {
        Ford = new Person("Ford");
        Prosser = new Person("Prosser");
        filths = new Filth();
        puddle = new Puddle();
    }
    
    public void Execute()
    {
        //Форд указал мистеру Проссеру на лужу
        Ford.PointToPuddle(puddle, Prosser);
        if (Prosser.IsNeedToGoSquah())
        {
            //и тот печально и неловко уселся в нее.
            prossersStatus = Prosser.GoSquash();
        }
        //Он чувствовал себя так, как будто..
        prossersFiilngs = Prosser.getFillings();
        //... и иногда он задумывался...
        Prosser.ThinkAboutDream(prossersDream);
        //Грязь облолакивала его седалище и руки и просачивалась в ботинки
        filths.EnvelopSeat(Prosser);
        filths.EnvelopHand(Prosser);
        filths.SeetIntoTheShoes(Prosser);
    }

    public Person getFord() {
        return Ford;
    }

    public Person getProsser() {
        return Prosser;
    }

    public Dream getProssersDream() {
        return prossersDream;
    }

    public Filth getFilts() {
        return filths;
    }

    public SeatingInPuddleStatus getProssersStatus() {
        return prossersStatus;
    }

    public Puddle getPuddle() {
        return puddle;
    }

    public Fillings getProssersFiilngs() {
        return prossersFiilngs;
    }
}
