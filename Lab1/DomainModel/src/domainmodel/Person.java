/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domainmodel;

/**
 *
 * @author Markeli
 */
public class Person {

    private Fillings fillings;
    private String name;
    private boolean isSeatEnvelopedByFilth;
    private boolean isHandEvelopedByFilth;
    private boolean isFilthSeepedIntoTheShoes;
    private boolean isNeedToGoSquah;
    private Puddle puddle;

    public Person(String name)
    {
        this.name = name+"!";
    }
    
    /**
     * @return the fillings
     */
    public Fillings getFillings() {
        return fillings;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the isSeatEnvelopedByFilth
     */
    public boolean IsSeatEnvelopedByFilth() {
        return isSeatEnvelopedByFilth;
    }

    /**
     * @param isSeatEnvelopedByFilth the isSeatEnvelopedByFilth to set
     */
    public void setIsSeatEnvelopedByFilth(boolean isSeatEnvelopedByFilth) {
        this.isSeatEnvelopedByFilth = isSeatEnvelopedByFilth;
    }

    /**
     * @return the isHandEvelopedByFilth
     */
    public boolean isHandEvelopedByFilth() {
        return isHandEvelopedByFilth;
    }

    /**
     * @param isHandEvelopedByFilth the isHandEvelopedByFilth to set
     */
    public void setIsHandEvelopedByFilth(boolean isHandEvelopedByFilth) {
        this.isHandEvelopedByFilth = isHandEvelopedByFilth;
    }

    /**
     * @return the isFilthSeepedIntoTheShoes
     */
    public boolean IsFilthSeepedIntoTheShoes() {
        return isFilthSeepedIntoTheShoes;
    }

    /**
     * @param isFilthSeepedIntoTheShoes the isFilthSeepedIntoTheShoes to set
     */
    public void setIsFilthSeepedIntoTheShoes(boolean isFilthSeepedIntoTheShoes) {
        this.isFilthSeepedIntoTheShoes = isFilthSeepedIntoTheShoes;
    }

    public void PointToPuddle(Puddle puddle, Person person)
    {
        person.setPuddle(puddle);
        person.setIsNeedToGoSquah(true);
    }
    
    public SeatingInPuddleStatus GoSquash()
    {
        puddle.setContent(this);
        fillings = Fillings.LifeIsDream;
        return new SeatingInPuddleStatus(false, false);
    }
    
    public void ThinkAboutDream(Dream dream)
    {
        dream = new Dream(this, false);
    }

    public boolean IsNeedToGoSquah() {
        return isNeedToGoSquah;
    }

    public Puddle getPuddle() {
        return puddle;
    }

    public void setPuddle(Puddle puddle) {
        this.puddle = puddle;
    }

    public void setIsNeedToGoSquah(boolean isNeedToGoSquah) {
        this.isNeedToGoSquah = isNeedToGoSquah;
    }
}
