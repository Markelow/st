/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domainmodel;

/**
 *
 * @author Markeli
 */
public class Puddle 
{
    private Person content;

    /**
     * @return the content
     */
    public Person getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(Person content) {
        this.content = content;
    }
    
}
