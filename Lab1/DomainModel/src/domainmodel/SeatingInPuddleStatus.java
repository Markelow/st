/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domainmodel;

/**
 *
 * @author Markeli
 */
public class SeatingInPuddleStatus 
{
    private boolean isComfortable;
    private boolean isSorrowfully;

    /**
     * @return the isComfortable
     */
    public boolean isIsComfortable() {
        return isComfortable;
    }

    /**
     * @param isComfortable the isComfortable to set
     */
    public void setIsComfortable(boolean isComfortable) {
        this.isComfortable = isComfortable;
    }

    /**
     * @return the isSorrowfully
     */
    public boolean isIsSorrowfully() {
        return isSorrowfully;
    }

    /**
     * @param isSorrowfully the isSorrowfully to set
     */
    public void setIsSorrowfully(boolean isSorrowfully) {
        this.isSorrowfully = isSorrowfully;
    }
    
    public SeatingInPuddleStatus(boolean isSorrowfully, boolean isComfortable)
    {
        this.isComfortable = isComfortable;
        this.isSorrowfully = isSorrowfully;
    }
    
}
