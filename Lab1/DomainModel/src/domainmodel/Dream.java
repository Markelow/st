/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domainmodel;

/**
 *
 * @author Markeli
 */
public class Dream 
{
    private Person owner;
    private boolean isOwnerLike;

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @return the isOwnerLike
     */
    public boolean isIsOwnerLike() {
        return isOwnerLike;
    }
    
    public Dream(Person owner, boolean isOwnerLie)
    {
        this.owner = owner;
        this.isOwnerLike = isOwnerLie;
    }
}
