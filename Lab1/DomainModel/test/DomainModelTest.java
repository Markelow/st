/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import domainmodel.DomainModel;
import domainmodel.Dream;
import domainmodel.Fillings;
import domainmodel.Person;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Markeli
 */
public class DomainModelTest 
{
    private static DomainModel domainModel;
    
    public DomainModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        domainModel = new DomainModel();
        domainModel.Execute();
    }
    
    @Test
    public void TestPuddlePart()
    {
        assertEquals("Ford name","Ford",domainModel.getFord().getName());
        assertEquals("Prosser name", "Prosser",domainModel.getProsser().getName());
        assertTrue("Squash pointer",domainModel.getProsser().IsNeedToGoSquah());
        
        assertFalse("Comfortable seating",domainModel.getProssersStatus().isIsComfortable());
        assertFalse("Sorrowfully seating",domainModel.getProssersStatus().isIsSorrowfully());
        
        
        assertEquals("Puddle content",domainModel.getProsser(), domainModel.getPuddle().getContent());
    }
    
    @Test
    public void TestThoughtsAndFillings()
    {        
        assertEquals("Fillings fail",Fillings.LifeIsDream, domainModel.getProssersFiilngs());
        Dream dream = domainModel.getProssersDream();
        boolean result = null == dream;
        assertTrue("No dreaming",result);
    }
    
    @Test
    public void TestFilth()
    {        
        Person Prosser = domainModel.getProsser();
        assertTrue("No filth on seat ",Prosser.IsSeatEnvelopedByFilth());
        assertTrue("No filt on hands",Prosser.isHandEvelopedByFilth());
        assertTrue("No filth on shoes",Prosser.IsFilthSeepedIntoTheShoes());
    }
}
