/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import RadixSorter.DemoRadixSorter;
import RadixSorter.RadixSorter;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Markeli
 */
public class RadixSortTest 
{
    
    public RadixSortTest() 
    {
    }
    
    @Test
    public void Test1NumArray() throws Exception
    {
        int[] array = {0, 9, 3, 2, 5, 7};
        DemoRadixSorter sorter = new DemoRadixSorter();
        
        sorter.Sort(array);        
        
        assertArrayEquals(new int[]{0,2,3,5,7,9}, array);
    }
    
    @Test
    public void Test2NumArray() 
    {
        int[] array = {17, 92, 90, 9, 99, 12, 22, 65, 55, 57, 73, 12};
        DemoRadixSorter sorter = new DemoRadixSorter();
        
        try
        {
            sorter.Sort(array);        
            Object[] logs = sorter.getLogArray().toArray();
            assertEquals("First digit sort","90 92 12 22 12 73 65 55 17 57 9 99 ", logs[0]);
            assertEquals("Second digit sort","9 12 12 17 22 55 57 65 73 90 92 99 ", logs[1]); 
        }
        catch (Exception e)
        {            
            assertFalse(true);
        }
    }
    
    @Test
    public void Test3NumArray() 
    {
        int[] array = {170, 92, 901, 9, 99, 142, 228, 651, 755, 574, 173, 12};
        DemoRadixSorter sorter = new DemoRadixSorter();
        try
        {
            sorter.Sort(array);        
            Object[] logs = sorter.getLogArray().toArray();
            assertEquals("First digit sort","170 901 651 92 142 12 173 574 755 228 9 99 ", logs[0]);
            assertEquals("Second digit sort","901 9 12 228 142 651 755 170 173 574 92 99 ", logs[1]); 
            assertEquals("Third digit sort","9 12 92 99 142 170 173 228 574 651 755 901 ", logs[2]);
        }
        catch (Exception e) 
        {
            assertFalse(true);
        }
    }
    
    @Test(expected=Exception.class)
    public void TestNull() throws Exception
    {        
        DemoRadixSorter sorter = new DemoRadixSorter();
        int[] temp = null;
        sorter.Sort(temp);
    }
}
