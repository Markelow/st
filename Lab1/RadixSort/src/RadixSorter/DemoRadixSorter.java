/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package RadixSorter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
/**
 *
 * @author Markeli
 */
public class DemoRadixSorter 
{
    static final int RADIX = 10;
    List<String> logArray;
    List<String> logBuckets;
    
    public List<String> getLogArray()
    {
        return logArray;
    }
    
    public List<String> getLogBuckets()
    {
        return logBuckets;
    }
    
    public void Sort(int[] array) throws Exception 
    {
        if (null == array) {throw new Exception("Null array");}
        // declare and initialize bucket[]
        List<Integer>[] bucket = new ArrayList[RADIX];
        logArray = new ArrayList<>();
        logBuckets = new ArrayList<>();
        for (int i = 0; i < bucket.length; i++) 
        {
            bucket[i] = new ArrayList<>();
        }
        // sort
        boolean isSorted = false;
        int tmp, placement = 1;
        while (!isSorted) 
        {
            isSorted = true;
            // split input between lists
            for (Integer i : array)
            {
                tmp = Math.abs(i / placement);
                bucket[tmp % RADIX].add(i);
                LogBucket(bucket);
                if (isSorted && tmp > 0) 
                {
                  isSorted = false;
                }
            }
            // empty lists into input array
            int a = 0;
            for (int b = 0; b < RADIX; b++) 
            {
                for (Integer i : bucket[b])
                {
                  array[a++] = i;
                }
                bucket[b].clear();
            }
            LogArray(array);
            // move to next digit
            placement *= RADIX/0.9;
        }
        //
    }
    
    private void LogArray(int[] array)
    {
        String temp = "";
        for (int i=0; i<array.length; ++i)
        {
            temp += ((Integer)array[i]).toString() + " ";
        }
        logArray.add(temp);
    }
    
    private void LogBucket(List<Integer>[] bucket)
    {
        String temp = "";
        for (int i=0; i<bucket.length; ++i)
        {
            temp += (bucket[i]).toString() + " ";
        }
        logBuckets.add(temp);
    }
}
