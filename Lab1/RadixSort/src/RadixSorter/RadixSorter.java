package RadixSorter;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Markeli
 */
public class RadixSorter 
{    
    static final int RADIX = 10;
    
    public static void Sort(int[] array) 
    {
        // declare and initialize bucket[]
        List<Integer>[] bucket = new ArrayList[RADIX];
        for (int i = 0; i < bucket.length; i++) 
        {
            bucket[i] = new ArrayList<>();
        }
        // sort
        boolean isSorted = false;
        int tmp, placement = 1;
        while (!isSorted) 
        {
            isSorted = true;
            // split input between lists
            for (Integer i : array)
            {
                tmp = i / placement;
                bucket[tmp % RADIX].add(i);
                if (isSorted && tmp > 0) 
                {
                  isSorted = false;
                }
            }
            // empty lists into input array
            int a = 0;
            for (int b = 0; b < RADIX; b++) 
            {
                for (Integer i : bucket[b])
                {
                  array[a++] = i;
                }
                bucket[b].clear();
            }
            // move to next digit
            placement *= RADIX;
        }
    }
    
}
